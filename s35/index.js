const express = require("express");

/*
	- Mongoose is a package that allows creation of Schemas to model our data structures.
	- Also, it has access to a number of methods for manipulating our data base
*/
const mongoose = require("mongoose");

const app = express();
const port = 3001;

// MongoDB Connection
/*
	- Connect to the database by passing in our connection string. 
		*Remember to replace the password and database names with actual values.

	Syntax:
		mongoose.connect("<MongoDB Connection String>, {useNewUrlParser:true}");
*/

// Connecting to MongoDB Atlas

mongoose.connect("mongodb+srv://admin123:admin123@project0.c7wlest.mongodb.net/s35?retryWrites=true&w=majority", 
	{
		// "{newUrlParser:true}" allows us to avoid any current and future errors while connecting to MongoDb
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

// Setup notifications for connectio success of failure
	// Connection to the database
	/*
		- Allows us to handle errors when the initial connection is established
		- Works with the "on" and "once" mongoose methods
	*/
	let db = mongoose.connection;

	db.on("error", console.error.bind(console, "connection error"));
	// If a connection error occurs, output in the console
	// "console.error.bind(console)" allowss us to print errors in our terminal

	db.once("open", () => console.log("We're connected to the cloud database"));

// Setup for allowing the server to handle data from requests
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Mongoose Schemas
/*
	- Schemas determine the structure of the documents to be written in the database
	- Schemas act as blueprints to our data
*/

// Use the "Schema()" constructor of the Mongoose module to create a new Schema object
// The "new" keyword creates a new Schema
const taskSchema = new mongoose.Schema({
	
	// define the fields with their corresponding data type
	name: String,
	status: {
		type: String,
		default: "pending"
	}
});

// Models
/*
	- Uses schemas and are used to create/instantiate objects that correspond to the schema
	- Server>Schema(blueprint)>Database>Collection
	- Models naming convention is that they must be in singular form and capitalized first letter
*/
const Task = mongoose.model("Task", taskSchema);

// Create New Task
	// Business Logic
	/*
		1. Add a functionality to check if there are duplicate tasks.
			- if the task already exists in the database, we return an error.
			- if the task does not exist in the database, we add it in the database.
		2. The task data will be coming from the request's body
		3. Create a new task object with a "name" field/property
		4. The "status" property does not need to be provided because our schema defaults it to "pending" open creation of an object
	*/

	app.post("/tasks", (request, response) => {
		Task.findOne({name:request.body.name}, (error, result) => {
			if(result != null && result.name == request.body.name){
				return response.send("Duplicate task found");
			} else {
				let newTask = new Task({
					name: request.body.name
				});
				newTask.save((saveError, savedTask)=>{
					if(saveError){
						return console.log(saveError);
					} else{
						return response.status(201).send("New task created");
					}
				});
			}
		});
	});

// Getting All The Tasks
	// Business Logic

	app.get("/tasks", (request, response) => {
		Task.find({}, (error, result) => {
			if(error){
				return console.log(error);
			} else{
				return response.status(200).json({
					data: result
				})
			}
		});
	});

// ----- ACTIVITY ----- //

// Creating the User Schema
const userSchema = new mongoose.Schema({
	username: String,
	password: String
});

// Creating the User Model
const User = mongoose.model("User", userSchema);

// Registering a user
	// Business Logic
	app.post("/signup", (request, response) => {
		User.findOne({name:request.body.name}, (error, result) => {
			if(result != null && result.username == request.body.username){
				return response.send("Duplicate user found.");
			} else {
				let newUser = new User({
					username: request.body.username,
					password: request.body.password
				});
				newUser.save((saveError, savedUser)=>{
					if(saveError){
						return console.log(saveError);
					} else{
						return response.status(201).send(`User ${request.body.username} successfully registered!`);
					}
				});
			}
		});
	});

// STRETCH-GOAL: Retrieving all users
	// Business Logic
	app.get("/users", (request, response) => {
		User.find({}, (error, result) => {
			if(error){
				return console.log(error);
			} else{
				return response.status(200).json({
					data: result
				})
			}
		});
	});

app.listen(port, () => console.log(`Server running at port ${port}`));